#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytz
from datetime import datetime

from flask_restful import Resource
from flask_restful import reqparse

import sqlalchemy as sa
from werkzeug.exceptions import NotFound
from werkzeug.exceptions import BadRequest

from src import models
from src.models import base
from src.common import logger as logging

logger = logging.get_logger()

parser = reqparse.RequestParser()
parser.add_argument('name', location='json', required=True)


class RoomBaseResource(object):

    @staticmethod
    def get_room(id):
        """Get a room based on the ID.

        Args:
            id (int): room id.

        Raises:
            Raise NotFound exception if the room was not found.

        Returns:
            If the room is valid, return the object.
        """
        try:
            return (
                base.session.query(models.Room)
                .filter_by(id=id)
                .filter_by(deleted_at=None)
                .one()
            )
        except sa.orm.exc.NoResultFound:
            logger.info('Room %r was not found', id)
            raise NotFound(f'The room {id} was not found')

    @staticmethod
    def check_room_exists(name):
        """Check if a given room name is unique.

        Args:
            name (basestring): room name.

        Raises:
            Raise BadRequest exception if there is another
            room with the same name.
        """
        try:
            room = (
                base.session.query(models.Room)
                .filter_by(name=name)
                .filter_by(deleted_at=None)
                .one()
            )
        except sa.orm.exc.NoResultFound:
            pass
        else:
            logger.info('Room %r has the same name: %r', room.id, room.name)
            raise BadRequest(f'There is another room called {name}')


class RoomResource(Resource, RoomBaseResource):

    def get(self):
        """Get all rooms."""
        query = (
            models.base.session.query(models.Room)
            .filter_by(deleted_at=None)
            .order_by(models.Room.id)
        )
        results = [dict(row) for row in query]
        if not results:
            raise NotFound('No room was found')
        return results

    def post(self):
        """Create a new room."""
        args = parser.parse_args()
        self.check_room_exists(args['name'])
        room = models.Room(name=args['name'])
        base.session.add(room)
        base.session.commit()
        logger.info('Room %r successfully created', room.id)
        return dict(room), 201


class SingleRoomResource(Resource, RoomBaseResource):

    def get(self, id):
        """Get a single room based on the id."""
        return dict(self.get_room(id))

    def put(self, id):
        """Update a single room based on the id."""
        args = parser.parse_args()
        self.check_room_exists(args['name'])
        room = self.get_room(id)
        room.name = args['name']
        base.session.add(room)
        base.session.commit()
        logger.info('Room %r successfully updated', id)
        return dict(room)

    def delete(self, id):
        """Delete a single room based on the id."""
        room = self.get_room(id)
        room.deleted_at = datetime.now(tz=pytz.utc)
        base.session.add(room)
        base.session.commit()
        logger.info('Room %r successfully deleted', id)
        return {}, 204
