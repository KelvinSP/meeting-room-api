#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytz
import functools
from datetime import datetime
from dateutil.parser import parse

from flask_restful import Resource
from flask_restful import reqparse

import sqlalchemy as sa
from werkzeug.exceptions import NotFound
from werkzeug.exceptions import BadRequest

from src import models
from src.models import base
from src.common import logger as logging
from src.resources import RoomBaseResource

logger = logging.get_logger()

# Arguments for POST and PUT
parser = reqparse.RequestParser()
parser.add_argument('title', location='json', required=True)
parser.add_argument('room_id', type=int, location='json', required=True)
parser.add_argument('start_time', location='json', required=True)
parser.add_argument('end_time', location='json', required=True)

# Arguments for GET
parser_get = reqparse.RequestParser()
parser_get.add_argument('room_id', type=int, location='args', required=False)
parser_get.add_argument('date', location='args', required=False)


def validate_room(func):
    """Validate that the room is valid."""
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        arguments = parser.parse_args()
        try:
            RoomBaseResource.get_room(arguments['room_id'])
        except NotFound:
            raise BadRequest('The selected room is not valid')
        else:
            return func(self, *args, **kwargs)
    return wrapper


class ScheduleBaseResource(object):

    MAXIMUM_HOURS_ALLOWED = 12

    @classmethod
    def validate_schedule(cls, schedule):
        """Check if the time range is valid.

        Args:
            schedule (model.Schedule): a schedule object.

        Raises:
            Raise BadRequest if the time range is invalid.
        """
        # Check if the end_time is earlier or equal to the start_time
        if schedule.end_time <= schedule.start_time:
            logger.info(
                'Invalid time range. End time is earlier than start time'
            )
            raise BadRequest(
                'The selected time range is not valid. '
                'The start time must be earlier than the end time.'
            )

        # Calculate the total seconds between the start time and end time
        seconds = (schedule.end_time - schedule.start_time).total_seconds()

        # Check if the amount of time does not exceed the max scheduling time
        if seconds > cls.MAXIMUM_HOURS_ALLOWED*60*60:
            logger.info('Invalid time range. Exceed MAXIMUM_HOURS_ALLOWED')
            raise BadRequest(
                f'The selected time range is not valid. '
                f'The maximum scheduling time for a room '
                f'is {cls.MAXIMUM_HOURS_ALLOWED} hours.'
            )

    @staticmethod
    def check_room_availability(schedule):
        """Check if the room is available for the time range.

        Args:
            schedule (model.Schedule): a schedule object.

        Raises:
            Raise BadRequest if there is another meeting in the same
            room in the same time.
        """
        query = (
            base.session.query(models.Schedule)
            .filter_by(room_id=schedule.room_id)
            .filter(schedule.start_time <= models.Schedule.end_time)
            .filter(models.Schedule.start_time <= schedule.end_time)
        )

        if schedule.id is not None:
            query = query.filter(models.Schedule.id != schedule.id)

        if query.count() > 0:
            logger.info(
                'There is another meeting in this room at the same time. '
                '%r - %r', schedule.start_time, schedule.end_time
            )
            raise BadRequest(
                'The selected time range is not valid. '
                'There is another meeting in this room at the same time.'
            )

    @staticmethod
    def get_schedule(id):
        """Get a specific schedule based on the ID.

        Args:
            id (int): schedule ID.

        Raises:
            Raise NotFound if the schedule was not found.

        Return:
            The model.Schedule object found.
        """
        try:
            return (
                base.session.query(models.Schedule)
                .filter_by(id=id)
                .filter_by(deleted_at=None)
                .one()
            )
        except sa.orm.exc.NoResultFound:
            logger.info('Schedule %r was not found', id)
            raise NotFound(f'The schedule {id} was not found')

    @staticmethod
    def set_schedule_args(schedule):
        """Set the args to the schedule object.

        Args:
            schedule (model.Schedule): a schedule object.

        Raises:
            Raise BadRequest if the datetime format is invalid.
        """
        args = parser.parse_args()
        schedule.title = args['title']
        schedule.room_id = args['room_id']
        try:
            schedule.start_time = parse(args['start_time'])
            schedule.end_time = parse(args['end_time'])
        except ValueError:
            logger.info(
                'Invalid datetime format: start time: %r - end time: %r',
                args['start_time'], args['end_time']
            )
            raise BadRequest('Invalid datetime format. Please use ISO 8601.')

    @staticmethod
    def get_schedules(room_id=None, date=None):
        """Get all schedules.

        Args:
            room_id (int): room ID.
            date (basestring): date in string format (e.g. 2018-10-10)

        Raises:
            Raise ValueError if the date is in an invalid format.

        Returns:
            Returns a list with all schedules found.
        """
        query = (
            models.base.session.query(models.Schedule)
            .filter_by(deleted_at=None)
            .order_by(models.Schedule.id)
        )

        # If available, filter by the room_id
        if room_id is not None:
            query = query.filter_by(room_id=room_id)

        # If available, filter by the date (based on the start_time)
        if date is not None:
            try:
                query = query.filter(
                    sa.func.DATE(models.Schedule.start_time) == parse(date)
                )
            except ValueError:
                logger.info('Invalid date: %r', date)
                raise BadRequest('Invalid date')

        return [dict(row) for row in query]


class ScheduleResource(Resource, ScheduleBaseResource):

    def get(self):
        """Get all schedules."""
        args = parser_get.parse_args()
        results = self.get_schedules(args['room_id'], args['date'])
        if not results:
            raise NotFound('No schedule was found')
        return results

    @validate_room
    def post(self):
        """Create a new schedule."""
        schedule = models.Schedule()
        self.set_schedule_args(schedule)
        self.validate_schedule(schedule)
        self.check_room_availability(schedule)
        base.session.add(schedule)
        base.session.commit()
        logger.info('Schedule %r successfully created', schedule.id)
        return dict(schedule), 201


class SingleScheduleResource(Resource, ScheduleBaseResource):

    def get(self, id):
        """Get a single schedule based on the id."""
        return dict(self.get_schedule(id))

    @validate_room
    def put(self, id):
        """Update a single schedule based on the id."""
        schedule = self.get_schedule(id)
        self.set_schedule_args(schedule)
        self.validate_schedule(schedule)
        self.check_room_availability(schedule)
        base.session.add(schedule)
        base.session.commit()
        logger.info('Schedule %r successfully updated', id)
        return dict(schedule)

    def delete(self, id):
        """Delete a single schedule based on the id."""
        schedule = self.get_schedule(id)
        schedule.deleted_at = datetime.now(tz=pytz.utc)
        base.session.add(schedule)
        base.session.commit()
        logger.info('Schedule %r successfully deleted', id)
        return {}, 204
