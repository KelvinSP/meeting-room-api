#!usr/bin/env python
# -*- encoding; utf-8 -*-

import logging


def get_logger(file_name='data'):
    formatter = logging.Formatter(
        fmt=(
            '%(asctime)s %(name)-8s %(filename)s:%(lineno)d '
            '%(levelname)-8s %(message)s'),
        datefmt='%Y-%m-%d %H:%M:%S')

    handlers = [
        logging.FileHandler('{}.log'.format(file_name), encoding='utf8'),
        logging.StreamHandler()]

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    if not len(logger.handlers):
        for handler in handlers:
            handler.setFormatter(formatter)
            handler.setLevel(logging.DEBUG)
            logger.addHandler(handler)

    return logger
