#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlalchemy as sa
from sqlalchemy import orm

from src.models import Base
from src.models import BaseModel


class Schedule(Base, BaseModel):
    """Model used to schedule a meeting for a specific room."""

    __tablename__ = 'schedule'

    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.Unicode(120), nullable=False)

    room_id = sa.Column(sa.Integer, sa.ForeignKey('room.id'), nullable=False)

    start_time = sa.Column(sa.DateTime(timezone=True), nullable=False)
    end_time = sa.Column(sa.DateTime(timezone=True), nullable=False)
