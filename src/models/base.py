#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import sqlalchemy as sa
from sqlalchemy.ext import declarative

from src.common import logger as logging

logger = logging.get_logger()

Base = declarative.declarative_base()


def get_engine_url(user, password, instance, port, db_name):
    """Get the engine URL based on the parameters.

    Args:
        user (str): database username.
        password (str): database password.
        instance (str): database instance.
        port (str): database port.
        db_name (str): database name.

    Returns:
        Return the formatted engine URL.
    """
    return 'postgresql://{user}:{password}@{instance}:{port}/{db_name}'.format(
        user=user,
        password=password,
        instance=instance,
        port=port,
        db_name=db_name
    )


def get_database_engine(master_access=False):
    """Create and return a database engine for connection.

    Args:
        master_access (bool): flag used to set the engine URL to use
        the production environment with master access. It can be used
        to run revisions in production and other sensitive procedures
        that requires master access.

    Returns:
        Return a database engine used for connection resources.
    """
    # TODO: the idea was to get these variables from a vault/secrets
    if master_access:
        logger.warning(
            'Attention: you are in production mode with master access'
        )
        engine_url = get_engine_url(
            os.environ['DB_MEETING_ROOM_MASTER_USER'],
            os.environ['DB_MEETING_ROOM_MASTER_PASS'],
            os.environ['DB_MEETING_ROOM_MASTER_INSTANCE'],
            os.environ['DB_MEETING_ROOM_MASTER_PORT'],
            os.environ['DB_MEETING_ROOM_MASTER_NAME']
        )
    else:
        engine_url = get_engine_url(
            os.environ.get('DB_MEETING_ROOM_USER', 'postgres'),
            os.environ.get('DB_MEETING_ROOM_PASS', 'postgres'),
            os.environ.get('DB_MEETING_ROOM_INSTANCE', 'localhost'),
            os.environ.get('DB_MEETING_ROOM_PORT', '5432'),
            os.environ.get('DB_MEETING_ROOM_NAME', 'meeting_room')
        )

    return sa.create_engine(engine_url)


def get_session(master_access=False):
    """Get a database scoped session based on the databas engine.

    Args:
        master_access (bool): flag used to set the engine URL to use
        the production environment.

    Returns:
        Return a SQLAlchemy scoped session using the database engine.
    """
    return sa.orm.scoped_session(
        sa.orm.sessionmaker(
            autoflush=False,
            autocommit=False,
            bind=get_database_engine(master_access=master_access)
        )
    )


session = get_session()
