#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

import sqlalchemy as sa


class BaseModel(object):
    """Class that provides common attributes to all model classes."""

    created_at = sa.Column(
        sa.TIMESTAMP(timezone=True),
        server_default=sa.func.now(),
        nullable=False
    )

    updated_at = sa.Column(
        sa.TIMESTAMP(timezone=True),
        server_default=sa.func.now(),
        onupdate=sa.func.current_timestamp(),
        nullable=False
    )

    deleted_at = sa.Column(sa.TIMESTAMP(timezone=True), nullable=True)

    def __iter__(self):
        """The dunder __iter__ allows us to convert a model  
        to a dictionary using for example `dict(room)`.
        """
        for col in sa.inspect(self).mapper.column_attrs:
            value = getattr(self, col.key)
            yield col.key, str(value) if isinstance(value, datetime) else value
