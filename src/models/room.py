#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlalchemy as sa
from sqlalchemy import orm

from src.models import Base
from src.models import BaseModel


class Room(Base, BaseModel):
    """Model used to represent a room."""

    __tablename__ = 'room'

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.Unicode(120), nullable=False)
