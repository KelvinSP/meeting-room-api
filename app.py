#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api

from src import resources
from src.models import base
from src.common import logger as logging

logger = logging.get_logger()

app = Flask(__name__)
api = Api(app)

api.add_resource(resources.RoomResource, '/api/room')
api.add_resource(resources.SingleRoomResource, '/api/room/<int:id>')

api.add_resource(resources.ScheduleResource, '/api/schedule')
api.add_resource(resources.SingleScheduleResource, '/api/schedule/<int:id>')


@app.teardown_request
def teardown_request(exception):
    """If some unexpected exception happens in any endpoint 
    this function ensures we run a rollback and will close 
    the database connection.
    """
    if exception is not None:
        logger.error('Unexpected error: %s', str(exception))
        base.session.rollback()
    base.session.close()


if __name__ == '__main__':
    app.run(debug=True)
