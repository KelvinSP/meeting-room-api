"""Add column deleted_at to the base model

Revision ID: f3f6918d8d06
Revises: 39f533a55494
Create Date: 2019-02-17 20:27:25.517500

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f3f6918d8d06'
down_revision = '39f533a55494'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'room', 
        sa.Column('deleted_at', sa.TIMESTAMP(timezone=True), nullable=True)
    )
    op.add_column(
        'schedule', 
        sa.Column('deleted_at', sa.TIMESTAMP(timezone=True), nullable=True)
    )


def downgrade():
    op.drop_column('schedule', 'deleted_at')
    op.drop_column('room', 'deleted_at')
