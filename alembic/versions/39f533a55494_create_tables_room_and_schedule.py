"""Create tables room and schedule

Revision ID: 39f533a55494
Revises: 
Create Date: 2019-02-17 17:14:52.425084

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '39f533a55494'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'room',
        sa.Column(
            'created_at', 
            sa.TIMESTAMP(timezone=True), 
            server_default=sa.text('now()'), 
            nullable=False
        ),
        sa.Column(
            'updated_at', 
            sa.TIMESTAMP(timezone=True), 
            server_default=sa.text('now()'), 
            nullable=False
        ),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.Unicode(length=120), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'schedule',
        sa.Column(
            'created_at', 
            sa.TIMESTAMP(timezone=True), 
            server_default=sa.text('now()'), 
            nullable=False
        ),
        sa.Column(
            'updated_at', 
            sa.TIMESTAMP(timezone=True), 
            server_default=sa.text('now()'), 
            nullable=False
        ),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('title', sa.Unicode(length=120), nullable=False),
        sa.Column('room_id', sa.Integer(), nullable=False),
        sa.Column('start_time', sa.DateTime(timezone=True), nullable=False),
        sa.Column('end_time', sa.DateTime(timezone=True), nullable=False),
        sa.ForeignKeyConstraint(['room_id'], ['room.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('schedule')
    op.drop_table('room')
