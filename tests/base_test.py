#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest.mock import Mock

import pytest

from app import app
from src.models import base


@pytest.fixture
def client():
    """Test client used to test a flask application."""
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def mock_database_commit(func):
    """Mock the database operations 'add' and 'commit'."""
    def wrapper(self, client, monkeypatch, *args, **kwargs):
        monkeypatch.setattr(base.session, 'add', Mock(return_code=None))
        monkeypatch.setattr(base.session, 'commit', Mock(return_code=None))
        return func(self, client, *args, **kwargs)
    return wrapper
