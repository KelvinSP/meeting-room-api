#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from datetime import datetime
from unittest import mock

import pytest
from werkzeug.exceptions import NotFound
from werkzeug.exceptions import BadRequest

from src import models
from src.resources.room import RoomBaseResource
from src.resources.schedule import ScheduleBaseResource

from tests.base_test import client
from tests.base_test import mock_database_commit


class TestSchedule(object):

    def mock_get_schedule(self, id):
        return models.Schedule(
            id=id,
            room_id=id,
            title=f'Title {id}',
            start_time='2019-02-20 10:00:00',
            end_time='2019-02-20 12:00:00',
            created_at='2019-02-20 10:00:00',
            updated_at='2019-02-20 10:00:00',
        )

    def mock_get_schedule_not_found(self, id):
        raise NotFound(f'The schedule {id} was not found')

    def test_validate_schedule_valid_parameters(self):
        schedule = models.Schedule(
            start_time=datetime(2019, 2, 20, 10, 0, 0),
            end_time=datetime(2019, 2, 20, 12, 0, 0),
        )
        assert ScheduleBaseResource.validate_schedule(schedule) is None

    def test_validate_schedule_invalid_start_time_and_end_time(self):
        schedule = models.Schedule(
            start_time=datetime(2019, 2, 20, 10, 0, 0),
            end_time=datetime(2019, 2, 10, 10, 0, 0),
        )
        with pytest.raises(BadRequest):
            ScheduleBaseResource.validate_schedule(schedule)

    def test_validate_schedule_invalid_scheduling_time(self):
        schedule = models.Schedule(
            start_time=datetime(2019, 2, 10, 10, 0, 0),
            end_time=datetime(2019, 2, 23, 10, 0, 0),
        )
        with pytest.raises(BadRequest):
            ScheduleBaseResource.validate_schedule(schedule)

    @mock.patch.object(ScheduleBaseResource, 'get_schedule', mock_get_schedule)
    def test_schedule_get_single_schedule_valid_arguments(self, client):
        response = client.get('/api/schedule/10')
        expected_response = {
            'id': 10,
            'room_id': 10,
            'title': 'Title 10',
            'start_time': '2019-02-20 10:00:00',
            'end_time': '2019-02-20 12:00:00',
            'created_at': '2019-02-20 10:00:00',
            'updated_at': '2019-02-20 10:00:00',
            'deleted_at': None,
        }
        assert response.status_code == 200
        assert json.loads(response.data) == expected_response

    @mock.patch.object(
        ScheduleBaseResource, 'get_schedule', mock_get_schedule_not_found
    )
    def test_schedule_get_single_schedule_valid_arguments(self, client):
        response = client.get('/api/schedule/10')
        expected_response = {'message': 'The schedule 10 was not found'}
        assert response.status_code == 404
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock.Mock())
    @mock.patch.object(ScheduleBaseResource, 'get_schedule', mock_get_schedule)
    @mock.patch.object(
        ScheduleBaseResource, 'check_room_availability', mock.Mock()
    )
    def test_schedule_put_single_schedule_valid_arguments(self, client):
        data = {
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2019-02-20 10:00:00',
            'end_time': '2019-02-20 12:00:00',
        }
        response = client.put('/api/schedule/20', json=data)
        expected_response = {
            'id': 20,
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2019-02-20 10:00:00',
            'end_time': '2019-02-20 12:00:00',
            'created_at': '2019-02-20 10:00:00',
            'updated_at': '2019-02-20 10:00:00',
            'deleted_at': None,
        }
        assert response.status_code == 200
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock.Mock())
    @mock.patch.object(ScheduleBaseResource, 'get_schedule', mock_get_schedule)
    def test_schedule_put_single_schedule_invalid_schedule(self, client):
        data = {
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2019-02-20 12:00:00',
            'end_time': '2019-02-20 10:00:00'
        }
        response = client.put('/api/schedule/20', json=data)
        expected_response = {
            'message': (
                'The selected time range is not valid. '
                'The start time must be earlier than the end time.'
            )
        }
        assert response.status_code == 400
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(ScheduleBaseResource, 'get_schedule', mock_get_schedule)
    def test_schedule_delete_single_schedule_valid_arguments(self, client):
        response = client.delete('/api/schedule/10')
        assert response.status_code == 204
        assert not response.data

    @mock.patch.object(
        ScheduleBaseResource, 'get_schedule', mock_get_schedule_not_found
    )
    def test_schedule_delete_single_schedule_not_found(self, client):
        response = client.delete('/api/schedule/10')
        expected_response = {'message': 'The schedule 10 was not found'}
        assert response.status_code == 404
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock.Mock())
    @mock.patch.object(
        ScheduleBaseResource, 'check_room_availability', mock.Mock()
    )
    def test_schedule_post_single_schedule_valid_arguments(self, client):
        data = {
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2019-02-20 10:00:00',
            'end_time': '2019-02-20 12:00:00',
        }
        response = client.post('/api/schedule', json=data)
        expected_response = {
            'id': None,
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2019-02-20 10:00:00',
            'end_time': '2019-02-20 12:00:00',
            'created_at': None,
            'updated_at': None,
            'deleted_at': None,
        }
        assert response.status_code == 201
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock.Mock())
    @mock.patch.object(ScheduleBaseResource, 'get_schedule', mock_get_schedule)
    def test_schedule_post_single_schedule_invalid_start_time(self, client):
        data = {
            'id': 20,
            'room_id': 20,
            'title': 'Standup Meeting',
            'start_time': '2010-20-20 20:20:20',
            'end_time': '2019-02-20 10:00:00',
        }
        response = client.post('/api/schedule', json=data)
        expected_response = {
            'message': 'Invalid datetime format. Please use ISO 8601.'
        }
        assert response.status_code == 400
        assert json.loads(response.data) == expected_response
