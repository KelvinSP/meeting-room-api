#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from unittest import mock

from werkzeug.exceptions import NotFound

from src import models
from src.resources.room import RoomBaseResource

from tests.base_test import client
from tests.base_test import mock_database_commit


class TestRoom(object):

    def mock_get_room(self, id):
        return models.Room(
            id=id,
            created_at='2019-02-20 10:00:00',
            updated_at='2019-02-22 10:00:00',
            name=f'Room {id}'
        )

    def mock_get_room_not_found(self, id):
        raise NotFound(f'The room {id} was not found')

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'check_room_exists', mock.Mock())
    def test_room_post_valid_arguments(self, client):
        response = client.post('/api/room', json={'name': 'Room 1'})
        expected_response = {
            'id': None,
            'created_at': None,
            'updated_at': None,
            'deleted_at': None,
            'name': 'Room 1'
        }
        assert response.status_code == 201
        assert json.loads(response.data) == expected_response

    def test_room_post_invalid_arguments(self, client):
        response = client.post('/api/room')
        expected_response = {
            'message': {'name': 'Missing required parameter in the JSON body'}
        }
        assert response.status_code == 400
        assert json.loads(response.data) == expected_response

    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room)
    def test_room_get_single_room_valid_arguments(self, client):
        response = client.get('/api/room/10')
        expected_response = {
            'id': 10,
            'created_at': '2019-02-20 10:00:00',
            'updated_at': '2019-02-22 10:00:00',
            'deleted_at': None,
            'name': 'Room 10'
        }
        assert response.status_code == 200
        assert json.loads(response.data) == expected_response

    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room_not_found)
    def test_room_get_single_room_invalid_arguments(self, client):
        response = client.get('/api/room/10')
        expected_response = {'message': 'The room 10 was not found'}
        assert response.status_code == 404
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room)
    @mock.patch.object(RoomBaseResource, 'check_room_exists', mock.Mock())
    def test_room_put_single_room_valid_arguments(self, client):
        response = client.put('/api/room/20', json={'name': 'Room Jedi'})
        expected_response = {
            'id': 20,
            'created_at': '2019-02-20 10:00:00',
            'updated_at': '2019-02-22 10:00:00',
            'deleted_at': None,
            'name': 'Room Jedi'
        }
        assert response.status_code == 200
        assert json.loads(response.data) == expected_response

    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room)
    def test_room_put_single_room_invalid_arguments(self, client):
        response = client.put('/api/room/20')
        expected_response = {
            'message': {'name': 'Missing required parameter in the JSON body'}
        }
        assert response.status_code == 400
        assert json.loads(response.data) == expected_response

    @mock.patch.object(RoomBaseResource, 'check_room_exists', mock.Mock())
    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room_not_found)
    def test_room_put_single_room_not_found(self, client):
        response = client.put('/api/room/10', json={'name': 'Room 1'})
        expected_response = {'message': 'The room 10 was not found'}
        assert response.status_code == 404
        assert json.loads(response.data) == expected_response

    @mock_database_commit
    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room)
    def test_room_delete_single_room_valid_arguments(self, client):
        response = client.delete('/api/room/10')
        assert response.status_code == 204
        assert not response.data

    @mock.patch.object(RoomBaseResource, 'get_room', mock_get_room_not_found)
    def test_room_delete_single_room_not_found(self, client):
        response = client.delete('/api/room/10')
        expected_response = {'message': 'The room 10 was not found'}
        assert response.status_code == 404
        assert json.loads(response.data) == expected_response
