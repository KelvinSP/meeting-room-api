# Meeting Room API

Book rooms for meetings using a simple Rest API.

This project uses [Python 3.7](https://www.python.org/downloads/release/python-370/), [PostgreSQL](https://www.postgresql.org/), [SQLAlchemy](https://www.sqlalchemy.org/), [Alembic](https://alembic.sqlalchemy.org/en/latest/), [Flask](http://flask.pocoo.org/), [Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/) and [Pytest](https://docs.pytest.org/en/latest/).

## Setting up the environment

Creating a virtual environment and installing the requirements:

```
sudo pip install virtualenv
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements-dev.txt
```

Creating the database:

Create a database called `meeting_room`, then set the following environment variables:

```
export DB_MEETING_ROOM_USER=postgres
export DB_MEETING_ROOM_PASS=postgres
export DB_MEETING_ROOM_INSTANCE=localhost
export DB_MEETING_ROOM_PORT=5432
export DB_MEETING_ROOM_NAME=meeting_room
```

Note that you must set your Postgres user and password.

And finally, at the root of the project, execute `alembic upgrade head` to run the revisions in your local database.

## Running the API

At the root of the project run `python app.py` and access the API at `http://localhost:5000/api`.

## HTTPIE examples

### Room

```
http GET http://localhost:5000/api/room
http POST http://localhost:5000/api/room <<< '{"name": "Sala 01 - Yoda"}'
http GET http://localhost:5000/api/room/1
http PUT http://localhost:5000/api/room/1 <<< '{"name": "Sala 01 - Jedi"}'
http DELETE http://localhost:5000/api/room/1
```

### Schedule

```
http GET http://localhost:5000/api/schedule
http GET http://localhost:5000/api/schedule room_id==1
http GET http://localhost:5000/api/schedule date=='2019-02-25'
http POST http://localhost:5000/api/schedule <<< '{"title": "Reunião de Code Review", "room_id": 1, "start_time": "2019-02-25 10:00:00.000000-03:00", "end_time": "2019-02-25 12:00:00.000000-03:00"}'
http GET http://localhost:5000/api/schedule/1
http PUT http://localhost:5000/api/schedule/1 <<< '{"title": "Reunião de Code Review", "room_id": 1, "start_time": "2019-02-25 12:00:00.000000-03:00", "end_time": "2019-02-25 14:00:00.000000-03:00"}'
http DELETE http://localhost:5000/api/schedule/1
```

## Running the tests

To run the tests we use `pytest` with [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/) for the tests coverage.
We can execute the tests running the following command at the root of the project:

```
pytest -vv --cov=src/ tests/
```

Note that the `-vv` is for verbose mode.

We can also manually test/consume the API using [HTTPie](https://httpie.org/), [CURL](https://curl.haxx.se/), [Postman](https://www.getpostman.com/), [Insomnia](https://insomnia.rest/) or any other tool.

## Important Notes

Some missing points which should be implemented before putting the project in production:

- To launch the application in production it would be needed to configure an application server, like [Gunicorn](https://gunicorn.org/), and another tool like [NGINX](https://www.nginx.com/) to deal with caching and load balancing.
- It would be nice to use some kind of [vault](https://www.vaultproject.io/) or [secrets](https://kubernetes.io/docs/concepts/configuration/secret/) to store and retrieve the database credentials.
- It would be awesome to have a CI/CD tool, like [Travis CI](https://travis-ci.org/), to automatically run a linter, run the tests and build and push the Docker image to a container registry.
- It would probably be a good idea to have integration tests. We could create a test database and perform real operations on it.
- Even if the API is for internal use, it is a great idea to have a system of authentication and authorization. It is important to control the access of some endpoints only to authorized people and keep track of which user has created/updated each record.
- Currently, the API is not using any kind of pagination. It would be nice to use pagination at least for the schedules, since it will probably have more records than room table. A great example is the [Github API pagination](https://developer.github.com/v3/#pagination).
- It would also be cool to run the API in a container in [Kubernetes](https://kubernetes.io/), since it is relatively easy to use a horizontal pod autoscaler there.
- The API was developed with [Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/) but another great options would be [Flask Potion](https://potion.readthedocs.io/en/latest/) and [Swagger](https://swagger.io/).

Any issues related to the project feel free to contact me at kelvinpfw@gmail.com.
